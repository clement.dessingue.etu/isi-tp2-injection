# Rendu "Injection"

## Binome

Nom, Prénom, email: Luquet, Amélie, amelie.luquet.etu@univ-lille.fr  
Nom, Prénom, email: Dessingué, Clément, clement.dessingue.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme?  
une fonction validate qui permet de vérifier le contenu de la chaine de caractères qu'on envoie

* Est-il efficace? Pourquoi?  
ce n'est pas efficace car dans la console on peut réécrire cette méthode pour faire passer n'importe quelle chaine

## Question 2

* Votre commande curl : <code>curl http://172.28.100.128:8080 --data-urlencode "';select * from chaines;"</code>


## Question 3

* Votre commande curl pour (effacer la table)
<code>curl http://172.28.100.128:8080 --data-urlencode "chaine=hello', 'nimp')#&submit=OK"</code>

* Expliquez comment obtenir des informations sur une autre table

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

## Question 5

* Commande curl pour afficher une fenetre de dialog. 

* Commande curl pour lire les cookies

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.


